#!/usr/bin/env python3.8
import sys
from collections import defaultdict
from textwrap import wrap

CHARS = ('a', 'b', 'c')
CHUNK_SIZE = 1000

chars_cnt = defaultdict(lambda: 0)
total_chars_cnt = defaultdict(lambda: 0)


def get_chat_cnt_str(chars_cnt_dict: dict) -> str:
    """
         Возвращает строку вида a: x, b: y, c: z где
        x - количество встретившихся символов a
        y - количество встретившихся символов b
        z - количество встретившихся символов c
    """
    res = ''
    for c in CHARS:
        res += f"{c}: {chars_cnt_dict[c]} "
    return res


def calc_chars(input_str: str) -> None:
    """
        Подсчет кол-ва символов в текущей строке и всего.
    """
    for char in CHARS:
        chars_cnt[char] = input_str.count(char)
        total_chars_cnt[char] += input_str.count(char)


def print_statistic() -> None:
    """
        Вывод статистики.
    """
    print()
    print(get_chat_cnt_str(chars_cnt))
    print(get_chat_cnt_str(total_chars_cnt))


if __name__ == '__main__':
    buf = ''
    while input_char := sys.stdin.read(1):
        if input_char == '\n':
            calc_chars(buf)
            print_statistic()
            continue
        buf += input_char
        if len(buf) == CHUNK_SIZE:
            calc_chars(buf)
            print_statistic()
            buf = ''
